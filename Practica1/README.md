### Universidad de San Carlos de Guatemala
### Facultad de Ingenieria
### Escuela de Ciencias y Sistemas
### Redes de Computadoras 2

### Practica 1
<br>

### Grupo 5:

|Nombre|Carnet|
|:---:|:---:|
|Juan José Hernandez Rodríguez|201612378|
|Fabio Andre Sanchez Chavez|201709075|
<br>
------------------------------------
# Descripcion del problema

### Olimpia Books, es una empresa que se dedica a la venta y distribución de libros en sus presentaciones como digitales, empastados duros, empastados blandos, entre otros. Hace poco tiempo se comenzó con la implementación de un modelode negocio basado en las ventas en línea, con lo cual sus ventas hanincrementado durante este tiempo de la pandemia. Sin embargo, con la llegada de pedidos mayores, su actual infraestructura no les provee una solución integral. Por ello, se le solicita a usted, experto en redes de computadoras, para brindarles asesoría e implementar una solución eficaz que pueda solventar las necesidades que actualmente requieren.
<br>

### Inicialmente solo se llevaba el registro de todos los pedidos en una máquina, la cual era utilizada por todas las personas que estaban a cargo de las ventas, distribución y administración. El plan de Olimpia Books, es manejar departamentos independientes y para ello se le indica que necesitan 2 o 3 personas en cada uno de los departamentos, cada uno con su respectiva máquina, pudiendo acceder al sistema. El dueño de Olimpia Books espera de su asesoría para poder implementar este sistema de la mejor manera posible.

<br>

# Topologia
![Topologia implementada](/Practica1/img/topo.jpg)  

<br>

# Segmentacion de la red

### Para segmentar la red se utilizo un subneteo estático de la red 192.168.15.0/24 y se dividio en 8 subredes. Cada subred cuenta con una mascara de red /27, se utilizaron 6 de las 8 subredes para los diferentes departamentos. A continuacion se detalla las subredes de cada departamento y sus vlan asignadas:
<br>

|VLAN|Subnet Address|Primera direccion asignable|Ultima direccion asignable|Broadcast Address|Mascara de subred|
|:--:|:--:|:--:|:--:|:--:|:--:|
|1|192.168.15.0|192.168.15.1 | 192.168.15.30|192.168.15.31|255.255.255.224|
|2|192.168.15.32|192.168.15.33 | 192.168.15.62|192.168.15.63|255.255.255.224|
|3|192.168.15.64|192.168.15.65 | 192.168.15.94|192.168.15.95|255.255.255.224|
|4|192.168.15.96|192.168.15.97 | 192.168.15.126|192.168.15.127|255.255.255.224|
|5|192.168.15.128|192.168.15.129 | 192.168.15.158|192.168.15.159|255.255.255.224|
|6|192.168.15.160|192.168.15.161 | 192.168.15.190|192.168.15.191|255.255.255.224|

---
<br>

# Configuracion de la red
### Se configuro cada uno de los dispositivos de la red, con los siguientes comandos:
## Switch0 (switch de capa 3), 

```
- Configuracion de VLANs:

    config t
    vlan 15
    name ventas
    exit
    vlan 25
    name distribucion
    exit
    vlan 35
    name administracion
    exit
    vlan 45
    name servidores
    exit
    vlan 99
    name management&native
    exit
    vlan 999
    name blackhole
    exit

- Configuracion de VTP:

    conf t
    vtp domain g5
    vtp password g5
    vtp mode server
    vtp version 2

- Configuracion interVLANs:

    interface vlan 15
    ip add 192.168.15.1 255.255.255.224
    no shutdown
    exit
    interface vlan 25
    ip add 192.168.15.33 255.255.255.224
    no shutdown
    exit
    interface vlan 35
    ip add 192.168.15.65 255.255.255.224
    no shutdown
    exit
    interface vlan 45
    ip add 192.168.15.97 255.255.255.224
    no shutdown
    exit
    config t
    ip routing
```
## Switch0 (switch de capa 2)
```
conf t

- Configuracion de enlaces troncales:

    int f0/7
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

    int range f0/1 - 6
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

- Configuracion de VTP:
    vtp domain g5
    vtp password g5
    vtp mode client
    vtp version 2

- Configuracion de portchannel:

    LACP:
    int range f0/1 - 2
    channel-protocol lacp
    channel-group 1 mode active
    exit

    int range f0/3 - 4
    channel-protocol lacp
    channel-group 2 mode active
    exit

    int range f0/5 - 6
    channel-protocol lacp
    channel-group 4 mode active
    exit
```

## Switch1 (switch de capa 2)
```
conf t

- Configuracion de enlaces troncales:

    int range f0/1 - 5
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

- Configuracion de VTP:

    vtp domain g5
    vtp password g5
    vtp mode client

- Configuracion de portchannel:
    LACP:
    int range f0/1 - 2
    channel-protocol lacp
    channel-group 1 mode active
    exit
```
## Switch2 (switch de capa 2)
```
conf f

- Configuracion de enlaces troncales:

    int range f0/1 - 4
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

- Configuracion de VTP:

    vtp domain g5
    vtp password g5
    vtp mode client

- Configuracion de portchannel:

    LACP:
    int range f0/1 - 2
    channel-protocol lacp
    channel-group 2 mode active
    exit

    int range f0/3 - 4
    channel-protocol lacp
    channel-group 3 mode active
    exit

- Configuracion de modos Acceso

    Ventas y Distribucion:

    interface range f0/5 - 6
    switchport mode access
    switchport access vlan 15
    exit

    interface range f0/7 - 8
    switchport mode access
    switchport access vlan 25
    exit
```
## Switch3 (switch de capa 2)
```
config t

- Configuracion de enlaces troncales:

    int range f0/1 - 4
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

- Configruacion de VTP:

    vtp domain g5
    vtp password g5
    vtp mode client

- Configuracion de portchannel:
    LACP:
    int range f0/1 - 2
    channel-protocol lacp
    channel-group 4 mode active
    exit

    int range f0/3 - 4
    channel-protocol lacp
    channel-group 3 mode active
    exit

- Configuracion de modo acceso

    Ventas y distribucion:

    interface f0/5
    switchport mode access
    switchport access vlan 15
    exit

    interface f0/6
    switchport mode access
    switchport access vlan 25
    exit
```

## Switch4 (switch de capa 2)
```
config t
- Configuracion de enlaces troncales:

    int f0/1
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

- Configuracion de VTP:

    vtp domain g5
    vtp password g5
    vtp mode client

- Configuracion de modo acceso

    Administracion y Servidores:

    interface f0/2
    switchport mode access
    switchport access vlan 35
    exit

    interface f0/3
    switchport mode access
    switchport access vlan 45
    interface range f0/7 - 8
    switchport mode access
    switchport access vlan 25
    exit
```
## Switch5 (switch de capa 2)
```
config t

- Configuracion de enlaces troncales:

    int f0/1
    switchport mode trunk
    switchport trunk allowed vlan 1,15,25,35,45,99,999,1002-1005
    exit

- Configuracion de VTP:

    vtp domain g5
    vtp password g5
    vtp mode client


- Configuracion de modo acceso

    Administracion y Servidores:

    interface f0/2
    switchport mode access
    switchport access vlan 35
    exit

    interface f0/3
    switchport mode access
    switchport access vlan 45
    exit
```
## Seguridad para interfaces
## SW0 capa3 
```
int range f0/3 - 24
switchport mode access
switchport access vlan 999
exit

config t
int range f0/1 - 2
switchport trunk native vlan 99
exit
```
## SW0 capa2
```
int range f0/8 - 24
switchport mode access
switchport access vlan 999
exit

int range f0/1 - 7
switchport trunk native vlan 99
exit
```
## SW1 
```
int range f0/6 - 24
switchport mode access
switchport access vlan 999
exit

int range f0/1 - 5
switchport trunk native vlan 99
exit
```
## SW2
```
int range f0/9 - 24
switchport mode access
switchport access vlan 999
exit

int range f0/1 - 4
switchport trunk native vlan 99
exit

int range f0/5 - 6
switchport port-security
switchport port-security maximum 5
switchport port-security violation shutdown
exit

int range f0/7 - 8
switchport port-security
switchport port-security maximum 1
switchport port-security mac-address sticky switchport port-security violation shutdown
exit
```
## SW3
```
int range f0/7 - 24
switchport mode access
switchport access vlan 999
exit

int range f0/1 - 4
switchport trunk native vlan 99
exit

int f0/5
switchport port-security
switchport port-security maximum 5
switchport port-security violation shutdown
exit

int f0/6
switchport port-security
switchport port-security maximum 1
switchport port-security mac-address sticky switchport port-security violation shutdown
exit
```
## SW4
```
int range f0/4 - 24
switchport mode access
switchport access vlan 999
exit

int f0/1
switchport trunk native vlan 99
exit

int f0/2
switchport port-security
switchport port-security maximum 5
switchport port-security violation shutdown
exit
```
## SW5
```
int range f0/4 - 24
switchport mode access
switchport access vlan 999
exit

int f0/1
switchport trunk native vlan 99
exit

int f0/2
switchport port-security
switchport port-security maximum 5
switchport port-security violation shutdown
exit

sh int f0/1 sw
```
# Configuracion VTP

    Dominio VTP: g5
    contraseña VTP: g5
    Version VTP: 2


# InterVLAN

### Se configuro el Switch de capa 3 a traves de ip routing las conexiones interVLAN, se asigno la primera direecion IP de cada subred para cada VLAN como default gateway.
<br>

# Elección de escenario con mejor resultado de convergencia

### Tras realizar los 4 escenarios posibles se realizo una comparacion de los resultados obtenidos y se selecciono el mejor resultado. La siguiente tabla resume los reusltados obtenidos:
<br>

|Escenario|Tipo|Protocolo|Tiempo de respuesta (s)|
|:---:|:---:|:---:|:---:|
|1|LACP|PVST|43|
|2|LACP|Rapid PVST|2|
|3|PAgP|PVST|58|
|4|PAgP|Rapid PVST|9|

<br>

Se ecoge para la solucion final el tipo de etherchannel LACP y el protocolo Rapid PVST ya que fue el que obtuvo el mejor resultado, fue el caso con menor tiempo de respuesta.
El peor caso fue el tipo de etherchannel PAgP y el protocolo PVST ya que el tiempo de respuesta fue de casi un minuto.

<br>

# Servidores web

### Los servidores web muestran una pagina web con la informacion de los integrantes del grupo:
<br>

![Pagagina web estatica](/Practica1/img/web.jpg) 